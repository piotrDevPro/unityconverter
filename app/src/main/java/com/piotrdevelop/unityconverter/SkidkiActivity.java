package com.piotrdevelop.unityconverter;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.math.BigDecimal;

public class SkidkiActivity extends AppCompatActivity implements TextWatcher, View.OnClickListener{

    Button myBtnClear;
    EditText edNum, edPerc, edResult,edResult2;
    TextView tv1,tv2,tv3;

    public BigDecimal roundUp(double value, int digits){
        return new BigDecimal(""+value).setScale(digits, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.skidki);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        Typeface myTypeface3 = Typeface.createFromAsset(getAssets(), "HelveticaNeueCyr-LightItalic.otf");
        Typeface myTypeface4 = Typeface.createFromAsset(getAssets(), "HelveticaNeueCyr-Italic.otf");

        myBtnClear = (Button) findViewById(R.id.myBtnClear);

        edNum = (EditText) findViewById(R.id.edNum);
        edPerc = (EditText) findViewById(R.id.edPerc);
        edResult = (EditText) findViewById(R.id.edResult);
        edResult2 = (EditText) findViewById(R.id.edResult2);

        tv1 = (TextView)findViewById(R.id.tv1);
        tv1.setTypeface(myTypeface3);
        tv2 = (TextView)findViewById(R.id.tv2);
        tv2.setTypeface(myTypeface3);
        tv3 = (TextView)findViewById(R.id.tv3);
        tv3.setTypeface(myTypeface4);

        edPerc.addTextChangedListener(this);
        edNum.addTextChangedListener(this);
        myBtnClear.setOnClickListener(this);
        edNum.requestFocus();


    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        double num1 = 0;
        double num2 = 0;
        double result = 0;
        double result2 = 0;

        if (TextUtils.isEmpty(edNum.getText().toString())

                || (edNum.getText().toString().equals('.')

                || TextUtils.isEmpty(edPerc.getText().toString())

                ||  (edPerc.getText().toString().equals('.')))){


            edResult.setText("");
            edResult2.setText("");
            return;
        }

        num1 = Double.parseDouble(edNum.getText().toString());{

            num2 = Double.parseDouble(edPerc.getText().toString());



            if (edNum.isFocused()) {

                result = ((num1 / 100) * num2);
                result2 = (num1 - result);

                edResult.setText(String.valueOf(roundUp(result,2)));
                edResult2.setText(String.valueOf(roundUp(result2,2)));

            }

            if (edPerc.isFocused()) {
                result = ((num1 / 100) * num2);
                result2 = (num1 - result);

                edResult.setText(String.valueOf(roundUp(result,2)));
                edResult2.setText(String.valueOf(roundUp(result2,2)));


            }

        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    @Override
    public void onClick(View v) {

        edNum.setText("");
        edPerc.setText("");
        edNum.requestFocus();




    }
}



