package com.piotrdevelop.unityconverter;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    Button btnDlinna, btnMassa, btnObiem, btnPloshad, btnRasxod, btnSpeed, btnTemp, btnHz, btnDavl, btnInfo, btnYgol, btnEnergy, btnKrut, btnPerc, btnObuv, btnSvet, btnTime, btnRad, btnKod, btnPlot, btnSila, btnProc, btnSkidki, btnBiz;
    TextView tvbase, tvEng, tveng2;
    // Toolbar my_toolbar;
    private Button btnClose;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


/*        Toolbar my_toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(my_toolbar);

        getSupportActionBar().setTitle(R.string.my_tb_title);
        getSupportActionBar().setSubtitle(R.string.my_sb_title);*/


        //      Intent myIntent = getIntent();
        //      if (Intent.ACTION_SEARCH.equals(myIntent.getAction())) {
        //          String query = myIntent.getStringExtra(SearchManager.QUERY);
        //          Intent ISR = new Intent(this, SearchResultActivity.class);
        //          startActivity(ISR);
        // Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
        //    }
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "HelveticaNeueCyr-Light.otf");
        Typeface myTypeface2 = Typeface.createFromAsset(getAssets(), "HelveticaNeueCyr-LightItalic.otf");

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        btnDlinna = (Button) findViewById(R.id.btnDlinna);
        btnDlinna.setTypeface(myTypeface);
        btnMassa = (Button) findViewById(R.id.btnMassa);
        btnMassa.setTypeface(myTypeface);
        btnObiem = (Button) findViewById(R.id.btnObiem);
        btnObiem.setTypeface(myTypeface);
        btnRasxod = (Button) findViewById(R.id.btnRasxod);
        btnRasxod.setTypeface(myTypeface);
        btnPloshad = (Button) findViewById(R.id.btnPloshad);
        btnPloshad.setTypeface(myTypeface);
        btnSpeed = (Button) findViewById(R.id.btnSpeed);
        btnSpeed.setTypeface(myTypeface);
        btnTemp = (Button) findViewById(R.id.btnTemp);
        btnTemp.setTypeface(myTypeface);
        btnHz = (Button) findViewById(R.id.btnHz);
        btnHz.setTypeface(myTypeface);
        btnTemp = (Button) findViewById(R.id.btnTemp);
        btnTemp.setTypeface(myTypeface);
        btnDavl = (Button) findViewById(R.id.btnDavl);
        btnDavl.setTypeface(myTypeface);
        btnInfo = (Button) findViewById(R.id.btnInfo);
        btnInfo.setTypeface(myTypeface);
        btnYgol = (Button) findViewById(R.id.btnYgol);
        btnYgol.setTypeface(myTypeface);
        btnEnergy = (Button) findViewById(R.id.btnEnegry);
        btnEnergy.setTypeface(myTypeface);
        btnKrut = (Button) findViewById(R.id.btnKrut);
        btnKrut.setTypeface(myTypeface);
        btnPerc = (Button) findViewById(R.id.btnPerc);
        btnPerc.setTypeface(myTypeface);
        btnObuv = (Button) findViewById(R.id.btnObyv);
        btnObuv.setTypeface(myTypeface);
        btnSvet = (Button) findViewById(R.id.btnSvet);
        btnSvet.setTypeface(myTypeface);
        btnTime = (Button) findViewById(R.id.btnTime);
        btnTime.setTypeface(myTypeface);
        btnRad = (Button) findViewById(R.id.btnRad);
        btnRad.setTypeface(myTypeface);
        btnKod = (Button) findViewById(R.id.btnKod);
        btnKod.setTypeface(myTypeface);
        btnPlot = (Button) findViewById(R.id.btnPlot);
        btnPlot.setTypeface(myTypeface);
        btnSila = (Button) findViewById(R.id.btnSila);
        btnSila.setTypeface(myTypeface);
        btnProc = (Button) findViewById(R.id.btnProc);
        btnProc.setTypeface(myTypeface);
        btnSkidki = (Button) findViewById(R.id.btnSkidki);
        btnSkidki.setTypeface(myTypeface);
        btnClose = (Button) findViewById(R.id.btnClose);
        btnClose.setTypeface(myTypeface);
        btnBiz = (Button) findViewById(R.id.btnBiz);
        btnBiz.setTypeface(myTypeface);
 /*       btnCurr = (Button)findViewById(R.id.btnCurr);
        btnCurr.setTypeface(myTypeface);*/

        //  btnsearch = (Button)findViewById(R.id.btnsearch);

        tvbase = (TextView) findViewById(R.id.tvbase);
        tvbase.setTypeface(myTypeface2);
        tvEng = (TextView) findViewById(R.id.tvEng);
        tvEng.setTypeface(myTypeface2);
        tveng2 = (TextView) findViewById(R.id.tveng2);
        tveng2.setTypeface(myTypeface2);


        btnDlinna.setOnClickListener(this);
        btnMassa.setOnClickListener(this);
        btnObiem.setOnClickListener(this);
        btnPloshad.setOnClickListener(this);
        btnRasxod.setOnClickListener(this);
        btnSpeed.setOnClickListener(this);
        btnTemp.setOnClickListener(this);
        btnHz.setOnClickListener(this);
        btnDavl.setOnClickListener(this);
        btnInfo.setOnClickListener(this);
        btnYgol.setOnClickListener(this);
        btnEnergy.setOnClickListener(this);
        btnKrut.setOnClickListener(this);
        btnPerc.setOnClickListener(this);
        btnObuv.setOnClickListener(this);
        btnSvet.setOnClickListener(this);
        btnTime.setOnClickListener(this);
        btnRad.setOnClickListener(this);
        btnKod.setOnClickListener(this);
        btnPlot.setOnClickListener(this);
        btnSila.setOnClickListener(this);
        btnProc.setOnClickListener(this);
        btnSkidki.setOnClickListener(this);
        btnClose.setOnClickListener(this);
        btnBiz.setOnClickListener(this);
/*        btnCurr.setOnClickListener(this);*/
        //   btnsearch.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);

//        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
//
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        switch (item.getItemId()){
//            case R.id.search:
//                Intent ISR = new Intent(this, SearchResultActivity.class);
//                startActivity(ISR);
//                break;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnDlinna:
                Intent intent = new Intent(this, DlinnaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnMassa:
                Intent MI = new Intent(this, MassaActivity.class);
                startActivity(MI);
                break;
            case R.id.btnObiem:
                Intent OI = new Intent(this, ObiemActivity.class);
                startActivity(OI);
                break;
            case R.id.btnPloshad:
                Intent PI = new Intent(this, PloshadActivity.class);
                startActivity(PI);
                break;
            case R.id.btnRasxod:
                Intent RI = new Intent(this, RasxodActivity.class);
                startActivity(RI);
                break;
            case R.id.btnSpeed:
                Intent SI = new Intent(this, SpeedActivity.class);
                startActivity(SI);
                break;
            case R.id.btnTemp:
                Intent TI = new Intent(this, TempActivity.class);
                startActivity(TI);
                break;
            case R.id.btnHz:
                Intent HI = new Intent(this, HerzActivity.class);
                startActivity(HI);
                break;
            case R.id.btnDavl:
                Intent DI = new Intent(this, DavlActivity.class);
                startActivity(DI);
                break;
            case R.id.btnInfo:
                Intent II = new Intent(this, InfoActivity.class);
                startActivity(II);
                break;
            case R.id.btnYgol:
                Intent YI = new Intent(this, YgolActivity.class);
                startActivity(YI);
                break;
            case R.id.btnEnegry:
                Intent EI = new Intent(this, EnergyActivity.class);
                startActivity(EI);
                break;
            case R.id.btnKrut:
                Intent KI = new Intent(this, KrutActivity.class);
                startActivity(KI);
                break;
            case R.id.btnPerc:
                Intent PEI = new Intent(this, PercActivity.class);
                startActivity(PEI);
                break;
            case R.id.btnObyv:
                showOnClickCloseObuv ();
               // Intent IO = new Intent(this, ObuvActivity.class);
              //  startActivity(IO);
                break;
            case R.id.btnSvet:
                Intent IS = new Intent(this, SvetActivity.class);
                startActivity(IS);
                break;
            case R.id.btnTime:
                Intent IT = new Intent(this, TimeActivity.class);
                startActivity(IT);
                break;
            case R.id.btnRad:
                Intent IR = new Intent(this, RadiationActivity.class);
                startActivity(IR);
                break;
            case R.id.btnKod:
                Intent IK = new Intent(this, KodActivity.class);
                startActivity(IK);
                break;
            case R.id.btnPlot:
                Intent IP = new Intent(this, PlotActivity.class);
                startActivity(IP);
                break;
            case R.id.btnSila:
                Intent ISI = new Intent(this, SilaActivity.class);
                startActivity(ISI);
                break;
            case R.id.btnProc:
                Intent IPR = new Intent(this, ProcentActivity.class);
                startActivity(IPR);
                break;
            case R.id.btnSkidki:
                Intent ISK = new Intent(this, SkidkiActivity.class);
                startActivity(ISK);
                break;

            case R.id.btnClose:

                showOnClickCloseDialog();

                break;
            case R.id.btnBiz:

                Intent IBIZ = new Intent(this, BizznessActivity.class);
                startActivity(IBIZ);

                break;

/*            case R.id.btnsearch:
                Intent ISR = new Intent(this, SearchResultActivity.class);
                startActivity(ISR);
                break;*/

        }

    }

    private void showOnClickCloseDialog() {


        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);


        final String[] items = {"Мужская/Верхняя (M/Top)", "Мужская/Джинсы(M/Jeans)", "Женская/Верхняя (W/Top)", "Женская/Джинсы(W/Jeans)", "Детская/Общий(Kids/Top)"};
        dialogBuilder.setTitle(R.string.clothes_dialog);
        dialogBuilder.setIcon(R.drawable.man);
        AlertDialog.Builder setItems = dialogBuilder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                switch (items[which]) {

                    case "Мужская/Верхняя (M/Top)":

                        Intent IMAN = new Intent(MainActivity.this, ManActivity.class);
                        startActivity(IMAN);

                        break;

                    case "Мужская/Джинсы(M/Jeans)":

                        Intent IML = new Intent(MainActivity.this, ActivityManLow.class);
                        startActivity(IML);

                        break;


                    case "Женская/Верхняя (W/Top)":

                        Intent IW = new Intent(MainActivity.this, WomanActivity.class);
                        startActivity(IW);

                        break;

                    case "Женская/Джинсы(W/Jeans)":

                        Intent IWL = new Intent(MainActivity.this, ActivityWomanLow.class);
                        startActivity(IWL);

                        break;
                    case "Детская/Общий(Kids/Top)":
                        Intent IKS = new Intent(MainActivity.this, KidsActivity.class);
                        startActivity(IKS);
                        break;

                }

            }


        });


        dialogBuilder.setPositiveButton("OK", null);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

    }


    private void showOnClickCloseObuv (){


        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);


        final String [] items = {"Обувь/Размер/Взрослая (Adult/Size)","Обувь/Размер/Детская (Kids/Size)"};
        dialogBuilder.setTitle(R.string.obuv_dialog);
        dialogBuilder.setIcon(R.drawable.shoes1);
        AlertDialog.Builder setItems = dialogBuilder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                switch (items[which]) {

                    case "Обувь/Размер/Взрослая (Adult/Size)":

                        Intent IO = new Intent(MainActivity.this, ObuvActivity.class);
                          startActivity(IO);

                        break;

                    case "Обувь/Размер/Детская (Kids/Size)":

                        Intent IKS = new Intent(MainActivity.this,ObyvKidsSize.class);
                        startActivity(IKS);

                        break;


                }

            }


        });


        dialogBuilder.setPositiveButton("OK", null);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

    }

}




